#!/bin/sh

#入退室の度にTweetするシェルスクリプト



##GPIOポートの設定

LED=4

PIR=25



##GPIOの値に応じてつぶやく内容を設定

message1="入室しました"

message2="退室しました"



##GPIOのモード設定

gpio -g mode $LED out

gpio -g mode $PIR in



##WAITの秒数
WAIT=45



##メインの部分

status1="0"

while true ; do

     ##センサの値を取得

     status2=`gpio -g read $PIR`



     ##センサが0から1になった場合

     if [ $status2 -eq 1 ] ; then

          if [ $status1 -eq 0 ] ; then

               status1="1"

               echo "入室したことをTweetしています．．．"

               gpio -g write $LED 1

               ttytter -ssl -status="$message1 `date +%H:%M`"

               echo "Tweetしました!"



          ##センサの値1から1になった場合

          elif [ $status1 -eq 1 ] ; then

              sleep 10

          fi



     ##センサが1から0になった場合

     elif [ $status2 -eq 0 ] ; then

          if [ $status1 -eq 1 ] ; then

               i=0

               while [ $i -ne $WAIT ] ; do

                   if [ `gpio -g read $PIR` -eq 0 ] ; then

                        i=`expr $i + 1`

                        echo "退室カウント $i /$WAIT"



                        sleep 1

                   elif [ `gpio -g read $PIR` -eq 1 ] ; then

                        i=0

                   fi

               done

               status1="0"

               echo "退室したことをTweetしています．．．"

               gpio -g write $LED 0

               ttytter -ssl -status="$message2 `date +%H:%M`"

               echo "Tweetしました!"

          elif [ $status1 -eq 0 ] ; then

               sleep 1

          fi

     fi

done
